#ifndef ApplicationClass_h__
#define ApplicationClass_h__

// System Includes
#include <iostream>
#include <memory>
#include <string>
#include <algorithm>

// External Dependencies
#include "tinyxml2.h"

// Namespaces
using namespace tinyxml2;
using namespace std;

// Application Includes
#include "LevelClass.h"
#include "PlayerClass.h"

class ApplicationClass
{
public:
	ApplicationClass();
	~ApplicationClass();
	void Update();
	
private:
	
	void checkCommand(string currentCommand);

	unique_ptr<LevelClass> m_level;
	shared_ptr<PlayerClass> m_player;

	string m_currentCommand;	//!< The most recent command entered by the user through the command prompt.
	std::vector<std::string> m_commands;	//!< A vector of all possible commands that can be accepted from the user.

	string m_commandType;
	string m_commandObject;
};

#endif // ApplicationClass_h__