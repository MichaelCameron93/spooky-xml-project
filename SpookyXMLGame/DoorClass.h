#ifndef DoorClass_h__
#define DoorClass_h__

class DoorClass
{
public:
	DoorClass(void);
	DoorClass(const char* name, const char* description);
	~DoorClass(void);

private:
	const char* m_name;
	const char* m_description;
};

#endif // DoorClass_h__
