#ifndef LevelClass_h__
#define LevelClass_h__

#include <iostream>
#include <vector>
#include <memory>

#include "tinyxml2.h"
using namespace tinyxml2;
using namespace std;

#include "RoomClass.h"


class LevelClass
{
public:
	LevelClass(void);
	LevelClass(const char* filename);
	~LevelClass(void);

private:
	//! Extract level data from the xml document and find any rooms in that level.
	//! Calls the loadRoom function for all rooms found.
	void loadLevel(XMLElement* const element);
	
	std::shared_ptr<RoomClass> loadRoom(XMLElement* const element);

	std::shared_ptr<ItemClass> loadItem(XMLElement* const element);
	std::shared_ptr<DoorClass> loadDoor(XMLElement* const element);
	
	XMLDocument m_levelXMLDoc;

	const char* m_name;

	vector<shared_ptr<RoomClass>> m_rooms;	//!< A vector of shared pointers containing each room in the level.

};

#endif // LevelClass_h__