#include <iostream>
#include <memory>
#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

#include "ApplicationClass.h"


unique_ptr<ApplicationClass> spookyGameApp; 

int main()
{

	spookyGameApp.reset(new ApplicationClass);
	while (true) {
		spookyGameApp->Update();
	}

    return 0;
}