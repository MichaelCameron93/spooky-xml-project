#ifndef ItemClass_h__
#define ItemClass_h__

class ItemClass
{
public:
	ItemClass(void);
	ItemClass(const char* name, const char* description);
	~ItemClass(void);

private:
	const char* m_name;
	const char* m_description;
};

#endif // ItemClass_h__