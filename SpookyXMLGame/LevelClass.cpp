#include "LevelClass.h"


LevelClass::LevelClass(void)
{
}

LevelClass::LevelClass(const char* filename)
{
	/*m_levelXMLDoc.LoadFile(filename);

	const char* levelName = m_levelXMLDoc.FirstChildElement( "Level" )->FirstChildElement( "Name" )->GetText();

	XMLElement* element = m_levelXMLDoc.FirstChildElement( "Level" )->FirstChildElement( "document" )->FirstChildElement( "English" );

	int value = element->IntAttribute( "value" );
	const char* name = element->Attribute( "name" );
	const char* description = element->GetText();

	std::cout << levelName << endl;
	std::cout << value << endl;
	std::cout << name << endl;
	std::cout << description << endl;*/

	m_levelXMLDoc.LoadFile(filename);

	
	XMLElement* element = m_levelXMLDoc.FirstChildElement( "level" );
	loadLevel(element);
}


LevelClass::~LevelClass(void)
{
}

void LevelClass::loadLevel(XMLElement* const element)
{
	m_name = element->FirstChildElement( "name" )->GetText();

	XMLElement* roomElement = element->FirstChildElement( "room" );
	while (roomElement) {
		m_rooms.emplace_back(loadRoom(roomElement));
		roomElement = roomElement->NextSiblingElement( "room" );
	}
}

std::shared_ptr<RoomClass> LevelClass::loadRoom(XMLElement* const element)
{
	const char* name = element->FirstChildElement( "name" )->GetText();
	const char* description = element->FirstChildElement( "description" )->GetText();

	std::cout << "-----------------------------------------" << endl;
	std::cout << endl << "room name: "<< name << endl;
	std::cout << "room description: " << description << endl << endl;

	std::shared_ptr<RoomClass> currentRoom = std::make_shared<RoomClass>(name, description);

	// Load Items
	XMLElement* itemElement = element->FirstChildElement( "item" );
	while (itemElement) {
		currentRoom->addItem(loadItem(itemElement));
		itemElement = itemElement->NextSiblingElement( "item" );
	}
	// Load Doors
	XMLElement* doorElement = element->FirstChildElement( "door" );
	while (doorElement) {
		currentRoom->addDoor(loadDoor(doorElement));
		doorElement = doorElement->NextSiblingElement( "door" );
	}
	return currentRoom;
}

std::shared_ptr<ItemClass> LevelClass::loadItem(XMLElement* const element)
{
	const char* name = element->FirstChildElement( "name" )->GetText();
	const char* description = element->FirstChildElement( "description" )->GetText();

	std::cout << "item name: " << name << endl;
	std::cout << "item description: " << description << endl << endl;

	std::shared_ptr<ItemClass> currentItem = std::make_shared<ItemClass>(name, description);

	return currentItem;
}
std::shared_ptr<DoorClass> LevelClass::loadDoor(XMLElement* const element)
{
	const char* name = element->FirstChildElement( "name" )->GetText();
	const char* description = element->FirstChildElement( "description" )->GetText();

	std::cout << "door name: " << name << endl;
	std::cout << "door description: " << description << endl << endl;

	std::shared_ptr<DoorClass> currentDoor = std::make_shared<DoorClass>(name, description);

	return currentDoor;
}