#ifndef RoomClass_h__
#define RoomClass_h__

#include <memory>
#include <vector>

#include "ItemClass.h"
#include "DoorClass.h"

class RoomClass
{
public:
	RoomClass(void);
	RoomClass(const char* name, const char* description);
	~RoomClass(void);

	//! Inserts an item at the end of the item vector.
	void addItem(std::shared_ptr<ItemClass> item);
	//! Inserts a door at the end of the door vector.
	void addDoor(std::shared_ptr<DoorClass> door);

private:
	std::vector<std::shared_ptr<DoorClass>> m_doors;	//!< A vector of pointers for storing the doors in a room.
	std::vector<std::shared_ptr<ItemClass>> m_items;	//!< A vector of pointers for storing the items in a room.

	const char* m_name;
	const char* m_description;
};

#endif // RoomClass_h__