#include "RoomClass.h"


RoomClass::RoomClass(void)
{
}

RoomClass::RoomClass(const char* name, const char* description)
	: m_name(name)
	, m_description(description)
{

}


RoomClass::~RoomClass(void)
{
}

void RoomClass::addItem(std::shared_ptr<ItemClass> item)
{
	m_items.emplace_back(item);
}

void RoomClass::addDoor(std::shared_ptr<DoorClass> door)
{
	m_doors.emplace_back(door);
}
