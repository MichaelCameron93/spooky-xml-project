#include "ApplicationClass.h"

using namespace std;
ApplicationClass::ApplicationClass(void)
{
	m_level.reset(new LevelClass("../Data/Levels/level_mansion.xml"));
	
	m_commands.push_back("show");
	m_commands.push_back("grab");
	m_commands.push_back("pick up");
	m_commands.push_back("take");
	m_commands.push_back("inspect");
	m_commands.push_back("look at");
}


ApplicationClass::~ApplicationClass(void)
{
}

void ApplicationClass::Update()
{
	std::cout << "What do? ";
	// Get command from user.
	std::getline (std::cin, m_currentCommand);
	// Convert to lowercase.
	std::transform(m_currentCommand.begin(), m_currentCommand.end(), m_currentCommand.begin(), std::tolower);
	// Extract the command type and object from the command.
	checkCommand(m_currentCommand);
	
}

void ApplicationClass::checkCommand(string currentCommand)
{
	m_commandType = "";
	m_commandObject = "";
	bool commandTypeFound = false;
	bool commandObjectFound = false;

	for(vector<string>::const_iterator iter = m_commands.begin(); iter != m_commands.end(); ++iter) {
		// Set command type and object to not be found.
		commandTypeFound	= false;
		commandObjectFound	= false;
		// Get the current command to be queried.
		const char* commandQuery = (*iter).c_str();
		// Find the string length of the command.
		int commandQueryLength = (*iter).length();
		// Try to find the command query in the command input by the user - i.e. show, look at, grab, etc.
		// Starting at the start of the command and ending at the length of the query command to filter out any rubbish.
		size_t found = currentCommand.find(commandQuery, 0, commandQueryLength);
		if ((found != string::npos) && (found < 1)) {
			// If a space is detected after the command to prompt an object
			// or if the command is the same length as the query command.
			size_t spaceFound = currentCommand.find(" ", commandQueryLength, 1);
			if(((spaceFound != string::npos) && ((int)spaceFound < (commandQueryLength + 1))) || 
				(currentCommand.length() == commandQueryLength)) {
				// Set the confirmed command type
				m_commandType = commandQuery;
				commandTypeFound = true;

				// If the command is found, search for any objects that could be used as part of a command.
				found = currentCommand.find(" ", commandQueryLength - 1);
				if (found != string::npos) {
					// Set the confirmed command object.
					// Copy the rest of the string into the object string.
					m_commandObject = currentCommand.substr(found + 1, string::npos);
					commandObjectFound = true;
				}
			}
		}
		if (commandTypeFound || (commandTypeFound && commandObjectFound))
			break;
	}

	std::cout << "command: \t" << m_commandType << endl;
	std::cout << "object: \t" << m_commandObject << endl << endl;
}


